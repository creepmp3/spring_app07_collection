/**
 * 2015. 6. 11.
 * @author KDY
 */
package kr.co.hbilab.dao;

import java.util.List;

import kr.co.hbilab.dto.BoardDTO;

public interface Dao {
    
    public int insertOne(BoardDTO dto);
    
    public List<BoardDTO> selectAll();
    
    public BoardDTO selectOne(int bno);
    
    public void readCountAdd(int bno);
    
    public void hitsCountAdd(int bno);
    
}
