package kr.co.hbilab.app;

import org.springframework.beans.factory.BeanFactory;
import org.springframework.beans.factory.xml.XmlBeanFactory;
import org.springframework.core.io.FileSystemResource;

public class TestMain {
    public static void main(String[] args) {
        BeanFactory factory = new XmlBeanFactory(new FileSystemResource("src/app.xml"));

        /*
        Object obj = factory.getBean("p");

        Printer p = (Printer) obj;
        for(int i = 0; i <= 10; i++) {
            p.printing(".");
        }
        */
        Printer p1 = factory.getBean("dot", Printer.class);
        Printer p2 = factory.getBean("dot", Printer.class);
        Printer p3 = factory.getBean("laser", Printer.class);
        System.out.println(p1);
        System.out.println(p2);
        p1.printing(".");
        p2.printing("-");
    }
}
